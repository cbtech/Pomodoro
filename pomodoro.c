#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>


const char *file = "/tmp/wtimer";

//const int work_time = 67; // Test 1 mins

const int rest_time = 600;  // Rest break 10 Mins

typedef struct {
	int start_time;
	char *state;
	int timer;
	int diff;
	int work_time; // 45 MINUTES = 46x60
} pomodoro;

// PROTO
void write_file(pomodoro *p);

int get_time()
{
	return time(NULL); 
}

int get_current_timer(pomodoro *p)
{
	return p->timer;
}

void decrement_timer(pomodoro* p)
{
	int current_time = get_time();
        p->diff = current_time - p->start_time;
	p->timer = p->work_time - p->diff;
}

void rest_break(pomodoro *p)
{ 	//1. TAKE 10 MIN BREAK
	//2. INCREMENT THE TIMER TO 45 MIN
	
	int current_time = get_time();
	p->diff = current_time - p->start_time ;
	printf("[ %s: %d ] Timer: %d",p->state,p->diff / 60,p->diff);

	if(p->diff == rest_time) {
		p->state = "WORK";
		p->start_time = get_time();
		p->diff = 0;
		write_file(p);
	}
}

void compute(pomodoro* p)
{
	if(!strcmp(p->state,"WORK")) {
		decrement_timer(p);
		printf("[ %s: %d Mins ]",p->state,p->timer /60);
		printf("Timer: %d",p->timer);
	}
	if(p->timer <= 50) p->state = "REST";
}

void write_file(pomodoro *p)
{
	FILE *fp = fopen(file,"w");
	
	fwrite(&p->start_time,sizeof(int),1,fp);
	fwrite(&p->state,sizeof(char),1,fp);
	fwrite(&p->work_time,sizeof(int),1,fp);
	fclose(fp);
}

void read_file(pomodoro *p)
{
	FILE *fp = fopen(file,"r");
	p->state = " ";
	fread(&p->start_time,sizeof(int),1,fp);
	fread(&p->state,sizeof(char),1,fp);
	fread(&p->work_time,sizeof(int),1,fp);

	fclose(fp);
}

void already_in_pause(pomodoro *p)
{
	read_file(p);
	if(!strcmp(p->state,"PAUSE"))
		p->state="RESUME";
	else { 
		p->state="PAUSE";
		p->work_time -= p->diff;
		
	}
}

int main(int argc,char *argv[])
{
	pomodoro p;
	int diff = 0;

	if (!strcmp(argv[1],"start")) {
		  p.start_time=get_time();
		  p.state = "WORK";
		  p.work_time= 2760;
		  write_file(&p);
	}

	else if (!strcmp(argv[1],"status")){
		read_file(&p);
		if(!strcmp(p.state,"WORK")) {
			compute(&p);
		}
		if(!strcmp(p.state,"REST")) {
			rest_break(&p);
			write_file(&p);
		}
		if(!strcmp(p.state,"PAUSE")) {
			printf("[ %s ]",p.state);
					   
		}
		if(!strcmp(p.state,"RESUME")) {
			p.state="WORK";
		 	write_file(&p);
		}
		//	write_file(&p);
	}
	else if (!strcmp(argv[1],"pause")) {
		already_in_pause(&p);
		write_file(&p);
	}

	return 0;
}
